#!/bin/bash

prefix="/"
destdir="/bitcoin/dist/robo-win64"

CONFIG_SITE="$PWD/depends/x86_64-w64-mingw32/share/config.site" \
    ./configure --without-libs --prefix="${prefix}"
make -j4

x86_64-w64-mingw32-strip "src/robod.exe"
x86_64-w64-mingw32-strip "src/robo-tx.exe"
x86_64-w64-mingw32-strip "src/qt/robo-qt.exe"
x86_64-w64-mingw32-strip "src/robo-cli.exe"
# x86_64-w64-mingw32-strip "src/.libs/libroboconsensus-0.dll"

make install DESTDIR="${destdir}"
