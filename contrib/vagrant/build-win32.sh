#!/bin/bash

prefix="/"
destdir="/bitcoin/dist/robo-win32"

CONFIG_SITE="$PWD/depends/i686-w64-mingw32/share/config.site" \
    ./configure --without-libs --prefix="${prefix}"
make -j4

i686-w64-mingw32-strip "src/robod.exe"
i686-w64-mingw32-strip "src/robo-tx.exe"
i686-w64-mingw32-strip "src/qt/robo-qt.exe"
i686-w64-mingw32-strip "src/robo-cli.exe"
# i686-w64-mingw32-strip "src/.libs/libroboconsensus-0.dll"

make install DESTDIR="${destdir}"
