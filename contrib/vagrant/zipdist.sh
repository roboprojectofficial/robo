#!/bin/bash

version="0.16.2"
pkgname="robo-${version}-linux"

set -e
cd dist

rm -f "robo-*-linux.*"
tar -zcvf "${pkgname}.tar.gz" "${pkgname}"

if [ ! -z "$GPG_KEY" ]; then
    gpg -u "$GPG_KEY" --output "${pkgname}.tar.gz.asc" --detach-sig "${pkgname}.tar.gz"
    gpg --verify "${pkgname}.tar.gz.asc" "${pkgname}.tar.gz"
fi
