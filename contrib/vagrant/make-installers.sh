#!/bin/bash

set -e

version="0.16.2"
PATH="/opt/wine/usr/bin:$PATH"
MAKENSIS="$HOME/.wine/drive_c/Program Files (x86)/NSIS/makensis.exe"

find dist -name "*.exe.bak" -delete
find dist -name "*.dll.bak" -delete
find . -name "robo*setup.exe" -delete
find . -name "robo*setup.asc" -delete

wine "$MAKENSIS" "contrib/vagrant/setup-32.nsi"
wine "$MAKENSIS" "contrib/vagrant/setup-64.nsi"

pkg32="dist/robo-${version}-win32-setup"
pkg64="dist/robo-${version}-win64-setup"

if [ ! -z "$GPG_KEY" ]; then
    gpg -u "$GPG_KEY" --output "${pkg32}.exe.asc" --detach-sig "${pkg32}.exe"
    gpg --verify "${pkg32}.exe.asc" "${pkg32}.exe"
    gpg -u "$GPG_KEY" --output "${pkg64}.exe.asc" --detach-sig "${pkg64}.exe"
    gpg --verify "${pkg64}.exe.asc" "${pkg64}.exe"
fi

exit 0
