#!/bin/bash

prefix="/usr/local"
version="0.16.2"
destdir="/bitcoin/dist/robo-${version}-linux"

CONFIG_SITE="$PWD/depends/x86_64-pc-linux-gnu/share/config.site" \
    ./configure --without-libs --prefix="${prefix}"
make clean && make -j4

strip "src/robod"
strip "src/robo-tx"
strip "src/qt/robo-qt"
strip "src/robo-cli"
# strip "src/.libs/libroboconsensus.so"

make install DESTDIR="${destdir}"
