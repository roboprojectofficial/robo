#!/bin/bash

version="0.16.2"
builder=`depends/config.guess`
site="$PWD/depends/${builder}/share/config.site"

if [ ! -f "$site" ]; then
    echo "Config site not found for OSX"
    echo "$site"
    exit 1
fi

CONFIG_SITE="${site}" CXXFLAGS="-fvisibility=hidden" ./configure --prefix="$HOME/.local"
make clean && make -j8 && make deploy
set -e
pkg=dist/robo-${version}-osx
rm -f ${pkg}.dmg.asc
mv ROBO-Qt.dmg ${pkg}.dmg
rm -rf ./ROBO*.app

if [ ! -z "$GPG_KEY" ]; then
    gpg -u "$GPG_KEY" --output "${pkg}.dmg.asc" --detach-sig "${pkg}.dmg"
    gpg --verify ${pkg}.dmg.asc ${pkg}.dmg
fi
