// Copyright (c) 2018 The ROBO developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#pragma once

#include <arith_uint256.h>
#include <chainparams.h>
#include <util.h>

bool CheckProofOfWorkGenesis(uint256 hash, unsigned int nBits, const Consensus::Params& params)
{
    bool fNegative;
    bool fOverflow;
    arith_uint256 bnTarget;

    bnTarget.SetCompact(nBits, &fNegative, &fOverflow);

    // Check range
    if (fNegative || bnTarget == 0 || fOverflow || bnTarget > UintToArith256(params.powLimit))
        return false;

    // Check proof of work matches claimed amount
    if (UintToArith256(hash) > bnTarget)
        return false;

    return true;
}

static void SolveGenesisBlock (CBlock& genesis, const Consensus::Params& params,
                               const std::string& network)
{
    printf("generating genesis block: %s\n", network.c_str());
    static const int nInnerLoopCount = 0x10000;
    const int64_t nStartTime = GetTime();
    genesis.nTime = GetTime();
    genesis.nNonce = 0;
    genesis.nBits = UintToArith256(params.powLimit).GetCompact();

    while (!CheckProofOfWorkGenesis(genesis.GetHashArgon2d(), genesis.nBits, params)) {
        if (genesis.nNonce % 1000 == 0)
            printf("hash: %s\n", genesis.GetHashArgon2d().ToString().c_str());
        ++genesis.nNonce;
        if (genesis.nNonce >= nInnerLoopCount) {
            genesis.nTime = GetTime();
            genesis.nNonce = 0;
        }
    }

    printf("\nGenesis Block: %s\n%s\n\n", network.c_str(), genesis.ToString().c_str());
    printf("elapsed: %d seconds\n", static_cast<int> (GetTime() - nStartTime));
    printf("hash: 0x%s\n", genesis.GetHash().ToString().c_str());
    printf("hash (pow): 0x%s\n", genesis.GetHashArgon2d().ToString().c_str());
    printf("merkle: 0x%s\n", genesis.hashMerkleRoot.ToString().c_str());
    printf("nBits: 0x%x\n", genesis.nBits);
    printf("nTime: %d\n", genesis.nTime);
    printf("nNonce: %d\n", genesis.nNonce);

    exit(1);
}
