
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <cstdlib>
#include "argon2/include/argon2.h"
#include "crypto/argon2.h"
#include "uint256.h"

static const char* POW_SECRET = "a5f20bc823ba7cb50e765b05723ded1ac1d065ec350b82a148c39a06ca8c2551";
static const size_t INPUT_BYTES = 80;   // Size of a block header in bytes.
static const size_t OUTPUT_BYTES = 32;  // Size of output needed for a 256-bit hash
static uint8_t* pArgon2Ad = nullptr;    // Pre allocated memory
static size_t nArgon2AdLen = 0;         // Pre allocated memory size

static uint32_t GetArgon2AdSize()
{
    return 1024 * 512;
}

static void UpdateArgon2AdValues()
{
    assert (pArgon2Ad != nullptr);
    for (size_t i = 0; i < nArgon2AdLen; ++i)
        pArgon2Ad[i] = static_cast<uint8_t> (i < 256 ? i : i % 256);
}

static void EnsureArgon2MemoryAllocated()
{
    const auto nSize = GetArgon2AdSize();
    if (nSize > nArgon2AdLen)
    {
        if (nullptr != pArgon2Ad)
            std::free (pArgon2Ad);
        pArgon2Ad = (uint8_t*) std::malloc (nSize);
        if (pArgon2Ad == nullptr)
            throw std::runtime_error("Could not allocate memory for argon2");
        nArgon2AdLen = nSize;
        UpdateArgon2AdValues();
    }
}

void Argon2Shutdown()
{
    nArgon2AdLen = 0;
    if (nullptr != pArgon2Ad)
    {
        std::free (pArgon2Ad);
        pArgon2Ad = nullptr;
    }
}

void Argon2dHash (const char* input, const char* output)
{
    EnsureArgon2MemoryAllocated();
    argon2_context ctx;

    ctx.version         = ARGON2_VERSION_13;
    ctx.flags           = ARGON2_DEFAULT_FLAGS;

    ctx.out             = (uint8_t*) output;
    ctx.outlen          = OUTPUT_BYTES;

    ctx.pwd             = (uint8_t*) input;
    ctx.pwdlen          = 56;
    ctx.salt            = ((uint8_t*) input) + ctx.pwdlen;
    ctx.saltlen         = INPUT_BYTES - ctx.pwdlen;

    ctx.secret          = (uint8_t*) POW_SECRET;
    ctx.secretlen       = strlen (POW_SECRET);
    ctx.ad              = pArgon2Ad;
    ctx.adlen           = GetArgon2AdSize();

    ctx.m_cost          = 512;
    ctx.t_cost          = 1;
    ctx.lanes           = 2;
    ctx.threads         = 1;

    ctx.allocate_cbk    = nullptr;
    ctx.free_cbk        = nullptr;

    assert (ctx.adlen > 0 && ctx.ad != nullptr);
    const int result = argon2_ctx (&ctx, Argon2_d);
    assert (result == ARGON2_OK);
}
