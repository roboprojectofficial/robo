// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <cstdint>

void Argon2dHash (const char* input, const char* output);
void Argon2Shutdown();
