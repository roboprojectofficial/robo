ROBO version 0.16.2 is now available

This is a new minor version release, with various bugfixes
as well as updated translations.

Please report bugs using the issue tracker at GitHub:

  <https://github.com/roboprojectofficial/robo/issues>

Credits
=======

Thanks to everyone who directly contributed to this release:
